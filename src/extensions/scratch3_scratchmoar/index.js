const ArgumentType = require('../../extension-support/argument-type')
const BlockType = require('../../extension-support/block-type')
const TargetType = require('../../extension-support/target-type')
const formatMessage = require('format-message')

class Scratch3Scratchmoar {
    constructor (runtime) {
        /**
         * Store this for later communication with the Scratch VM runtime.
         * If this extension is running in a sandbox then `runtime` is an async proxy object.
         * @type {Runtime}
         */
        this.runtime = runtime
    }

    /**
     * @return {object} This extension's metadata.
     */
    getInfo () {
        return {
            id: 'Scratch3Scratchmoar',
            name: 'Moar',
            color1: '#b06700',
            color2: '#774806',
        }
    }
}

module.exports = Scratch3Scratchmoar